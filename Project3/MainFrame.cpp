﻿#include "MainFrame.h"

enum id {
	addButtonId = wxID_HIGHEST + 1,
	completeTodoButtonId = wxID_HIGHEST + 2
};


MainFrame::MainFrame(wxString title, wxPoint position, wxSize size)
	: wxFrame(NULL, wxID_ANY, title, position, size)
{
	rootBoxSizer = new wxBoxSizer(wxVERTICAL);
	addTaskBoxSizer = new  wxBoxSizer(wxHORIZONTAL);

	datepickerCtrl = new wxDatePickerCtrl(this, -1);
	taskTextCtrl = new wxTextCtrl(this, -1);
	addButton = new wxButton(this, addButtonId, "Add task");
	completeTodoButton = new wxButton(this, completeTodoButtonId, "Complete");

	todoList = new wxListCtrl(this, -1, wxDefaultPosition, wxDefaultSize);

	addTaskBoxSizer->Add(taskTextCtrl, wxEXPAND | wxTOP);
	addTaskBoxSizer->Add(datepickerCtrl);
	addTaskBoxSizer->Add(addButton);

	rootBoxSizer->Add(addTaskBoxSizer, 0, wxEXPAND | wxTOP);
	rootBoxSizer->Add(todoList, 1, wxEXPAND);

	rootBoxSizer->Add(completeTodoButton, 0, wxEXPAND);

	SetSizer(rootBoxSizer);
}

void MainFrame::AddTaskClick(wxCommandEvent& event)
{
	//Збираємо текс для тодушки
	wxString todoText = taskTextCtrl->GetValue();

	//Перевірка на пустоту
	if (todoText.IsEmpty())
	{
		wxLogMessage("Text cannot be empty");
		return;
	}


	wxDateTime dateTime = datepickerCtrl->GetValue();
	wxString dateTimeStr = dateTime.Format("%y %m %d");
	todoText.Append(dateTimeStr);


	//Додаємо тодушку в список
	wxListItem titleItem;
	titleItem.SetText(todoText);
	titleItem.SetId(todoId);
	todoList->InsertItem(titleItem);
	todoId++;

	//Очищаємо контроли
	taskTextCtrl->Clear();
}

void MainFrame::CompleteTodoClick(wxCommandEvent& event)
{
	long item = -1;
	for (;; )
	{
		item = todoList->GetNextItem(item,
			wxLIST_NEXT_ALL,
			wxLIST_STATE_SELECTED);
		if (item == -1)
			break;
		todoList->DeleteItem(item);
		todoList->Arrange(0);
	}
}

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_BUTTON(addButtonId, MainFrame::AddTaskClick)
EVT_BUTTON(completeTodoButtonId, MainFrame::CompleteTodoClick)
END_EVENT_TABLE()
