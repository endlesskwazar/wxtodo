#include "ToDoApp.h"
#include "MainFrame.h"


bool ToDoApp::OnInit()
{
	MainFrame* mainFrame = new MainFrame("ToDoApp", wxDefaultPosition, wxDefaultSize);
	mainFrame->Show(true);
	SetTopWindow(mainFrame);
	return true;
}

wxIMPLEMENT_APP(ToDoApp);