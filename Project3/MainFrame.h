#pragma once
#include <wx/wx.h>
#include <wx/datectrl.h>
#include <wx/listctrl.h>

class MainFrame : public wxFrame
{
public:
	MainFrame(wxString title, wxPoint position, wxSize size);

	wxBoxSizer* rootBoxSizer;
	wxBoxSizer* addTaskBoxSizer;
	wxTextCtrl* taskTextCtrl;
	wxDatePickerCtrl* datepickerCtrl;
	wxButton* addButton;
	wxListCtrl* todoList;
	int todoId = 0;
	wxButton* completeTodoButton;

	void AddTaskClick(wxCommandEvent& event);
	void CompleteTodoClick(wxCommandEvent& event);

	DECLARE_EVENT_TABLE();
};

